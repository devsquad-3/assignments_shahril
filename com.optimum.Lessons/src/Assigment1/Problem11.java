package Assigment1;//removing duplicate elements in an array

public class Problem11 {

	public static void main(String[] args) {
		int x[] = {1,2,3,1,2,3,4};
		System.out.print("Before Sorting: ");
		for(int i = 0; i<x.length; i++) {
			System.out.print(x[i]+ " ");
		}
		System.out.print("\nAfter Sorting: ");
		for (int i = 0; i < x.length; i++) {
			for (int j = i; j < x.length; j++) {
				if (x[i]>x[j]) {
					int temp = x[i];
					x[i] = x[j];
					x[j] = temp;
					
				}
				
			}
			
		}
		for (int i = 0; i < x.length; i++) {
			System.out.print(x[i]+ " ");
			
		}
		System.out.print("\nAfter removing duplicates: ");
		int b = 0;
		x[b] = x[0];
		for (int i = 0; i < x.length; i++) {
			if (x[b] != x[i]) {
				b++;
				x[b]=x[i];
				
			}
			
		}
		for (int i = 0; i <=b; i++) {
			System.out.print(x[i] + " ");
			
		}

	}

}

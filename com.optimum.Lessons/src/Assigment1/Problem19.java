package Assigment1;

import java.util.Scanner;

public class Problem19 {

	public static void main(String[] args) {
		int num = 0;
		int reversedNum = 0;
		int remainder = 0;//declaring variables
		 Scanner refScanner = new Scanner(System.in);
		 System.out.println("Key in any number to check if Palindrome or not ");//asking user for input
		 num = refScanner.nextInt();//user can only input int
		 refScanner.close();//closing scanner to prevent leaks
		
		
		  //storing user input into originalNum 
		 int originalNum = num;
		  
		  //USING WHILE LOOP //acquiring reverse of originalNum and storing it in a   variable
		 while (num != 0) { 
			 remainder = num % 10; 
			 reversedNum = reversedNum *10 + remainder; 
			 num /= 10; 
	} 
		 
		 if (originalNum == reversedNum) { //to check if reverseNum and originalNum is equal
			 System.out.println(originalNum +" is Palindrome");
		  
		  } else { System.out.println(originalNum + " is not a Palindrome");
		  
		  }
		 
		 
		/*
		 * //USING FOR LOOP
		 * 
		 * int sum = 0; for (;num != 0; num /= 10) { remainder = num%10; sum =
		 * (sum*10)+remainder; num = num/10;
		 * 
		 * } if (sum == num) { System.out.println("Number is a Palindrome");
		 * 
		 * } else { System.out.println("Number is not a Palindrome");
		 * 
		 * }
		 */
	}

}

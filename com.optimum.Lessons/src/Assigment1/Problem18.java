package Assigment1;

import java.util.Scanner;

public class Problem18 {

	public static void main(String[] args) {
		int oN = 0;
		int r = 0;
		int rt = 0;
		int n = 0;//declaring variables
		 Scanner refScanner = new Scanner(System.in);
		 System.out.println("Key in 3 digit numbers which you think is an armstrong number! ");//asking user for input
		 n = refScanner.nextInt();//user can only input int
		 refScanner.close();//closing input to prevent leaks
		
		oN = n;
		
		while (oN != 0) {//condition set for logic
			r = oN % 10;
			rt += Math.pow(r, 3);
			oN /= 10;// end of logic/ equation to determine armstrong number
			
		}
		if (rt == n) {
			System.out.println(n + " is an armstrong number ");
			
		} else {
			System.out.println(n + " is NOT an armstrong number ");

		}

	}

}

package Assigment1;

import java.util.Scanner;

public class Problem20 {
	// Function that returns true if
    // str is a palindrome
    static boolean isPalindrome(String str)
    {
 
        // Pointers pointing to the beginning
        // and the end of the string
        int i = 0, j = str.length() - 1;
 
        // While there are characters to compare
        while (i < j) {
 
            // If there is a mismatch
            if (str.charAt(i) != str.charAt(j))
                return false;
 
            // Increment first pointer and
            // decrement the other
            i++;
            j--;
        }
 
        // Given string is a palindrome
        return true;
    }

	public static void main(String[] args) {
		String str = "";
		String reverseStr = "";
		
		 Scanner refScanner = new Scanner(System.in);
		 System.out.println("Key in any String to determine if its Palindrome or not ");//asking user for input
		 str = refScanner.findInLine(str);//user can only input Strings
		 refScanner.close();//close scanner to prevent leaks
	
		int strLength = str.length();//letting strLength find the actual length of the string and converting to int
		
		for (int i = (strLength -1); i >= 0; i--) {
			reverseStr = reverseStr + str.charAt(i);//converting the string to to char
			
		}
		if (str.toLowerCase().equals(reverseStr.toLowerCase())) {//condition set so that str and reverseStr is the same value and also to lowercase
			System.out.println("Yup its a Palindrome String");
			
		} else {
			System.out.println("Nope its not a Palindrome String");

		}
		if (isPalindrome(str))
            System.out.print("Yes");
        else
            System.out.print("No");

	}

}

package Assigment1;

import java.util.Arrays;
import java.util.Scanner;

public class Problem12 {
	public static int getSecondSmallest(int[] a, int total) {
		int b = 0;
		a[b] = a[0];
		for (int i = 0; i < a.length; i++) {
			if (a[b] != a[i]) {
				b++;
				a[b] = a[i];
				
			}
			
		}
		int temp;
		for (int i = 0; i < total; i++) {
			for (int j = 1+1; j < total; j++) {
				if (a[i] > a[j] ) {
					temp = a[i];
					a[i] = a[j];
					a[j] = temp;
					
				}
				
			}
			
		}
		return a[1];
	}
	
	public static int getSecondLargest(int[] a, int total) {
		int b = 0;
		a[b] = a[0];
		for (int i = 0; i < a.length; i++) {
			if (a[b] != a[i]) {
				b++;
				a[b]=a[i];
				
				
			}
			
		}
		int temp;
		for (int i = 0; i < total; i++) {
			for (int j = i+1; j < total; j++) {
				if (a[i] > a[j]) {
					temp = a[i];
					a[i] = a[j];
					a[j] = temp;
					
				}
				
			}
			
		}
		return a[total-2];
	}

	public static void main(String[] args) {
		int[] x = {1,3,7,3,5,6,8,8};
		Arrays.sort(x);
		System.out.println("Sorting Array X ====>" + Arrays.toString(x)+ "<==== Array X Sorted");
		System.out.println("Second Smallest in Array X is " + getSecondSmallest(x, x.length));
		System.out.println("Second Largest in Array X is " + getSecondLargest(x, x.length));
	}

}

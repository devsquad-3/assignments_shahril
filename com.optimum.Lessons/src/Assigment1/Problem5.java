package Assigment1;

import java.util.Scanner;

public class Problem5 {

	public static void main(String[] args) {
		int i,j;
		 Scanner refReader = new Scanner(System.in);
		 System.out.println("enter number of row");
		 int x = Integer.parseInt(refReader.nextLine());
		 System.out.println("Enter number");
		 String y =refReader.next();
		 refReader.close();
		 
		 for(i = x; i >= 0; i--){//for loop which takes user input then does decrement of 1 per column
		        
		        for(j = 1; j <= (1 + i - 1); j++){//nested for loop that uses user input and does
		        	//decrement by 1 per row also decreasing the value per row
		            System.out.print(""+j);
		        }
		        System.out.println();
		    } 
		}
}

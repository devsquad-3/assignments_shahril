package Assigment1;

import java.math.BigInteger;
import java.util.Scanner;


public class Problem17 {//find factorial of a given number

	public static void main(String[] args) {
		
		 int i = 1; int f = 1; int n = 0;//declaring the variables
		 
		 System.out.println("Key in number in which to find the factorial ");//ask user for factorial number input
		 Scanner refScanner = new Scanner(System.in);
		 n = refScanner.nextInt();//declaring user input as int
		 
		 // USING WHILE LOOP
		 /* while (i <= n) {//creating while loop thru the factorial of the number f = f
		 * * i; i++;
		 * 
		 * } System.out.println("Factorial of the number that you keyed in is : " + f);
		 */
		
		/*
		 * //USING FOR LOOP
		 *  for (int j = 1; j <= n; j++) { f = f*j; }
		 * System.out.print("Factorial of " + n + " is: " + f);
		 */
		 
		 /*USING BIGINTEGER
		  *BigInteger f1 = BigInteger.ONE; for (int j = 1; j <= n; j++) { f1 =
		  *f1.multiply(BigInteger.valueOf(j)); } System.out.print("Factorial of " + n +
		  *" is: " + f1);
		  */
		 
		 //USING RECURSION
		long f2 = multiplyNumber(n);
		System.out.println("Factorial of " + n + " is " + f2);
	}
          public static long multiplyNumber(int n) {
        	if (n >= 1) {
        		return n * multiplyNumber(n - 1);
        	}
				return 1;
			}
}
